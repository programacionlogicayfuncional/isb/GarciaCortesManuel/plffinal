(ns plffinal.core)



(defn regresa-al-punto-de-origen?
  [s]
  (letfn [(h [cx x]
             (cond
               (= cx \>)(inc x)
               (= cx \<)(dec x)
               :else x))
          (i [cy y]
             (cond 
               (= cy \^)(inc y)
               (= cy \v)(dec y)
               :else y))
          (g [a b]
             (== a b))
          (f [xs x y]
             (if (empty? xs)
               (g x y)
               (f (rest xs) (h (first xs) x) (i (first xs) y))))]
    (f s 0 0)))




(regresa-al-punto-de-origen? [\> \<])
(regresa-al-punto-de-origen? (list \> \<))
(regresa-al-punto-de-origen? "v^")
(regresa-al-punto-de-origen? [\v \^])
(regresa-al-punto-de-origen? "^>v<")
(regresa-al-punto-de-origen? (list \^ \> \v \<))
(regresa-al-punto-de-origen? "<<vv>>^^")
(regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])

(regresa-al-punto-de-origen? ">")
(regresa-al-punto-de-origen? (list \>))
(regresa-al-punto-de-origen? "<^")
(regresa-al-punto-de-origen? [\< \^])
(regresa-al-punto-de-origen? ">>><<")
(regresa-al-punto-de-origen? (list \> \> \> \< \<))
(regresa-al-punto-de-origen? [\v \v \^ \^ \^])


(defn regresan-al-punto-de-origen?
  [& xs]
  (cond
    (nil? xs) true
    :else
    (every? regresa-al-punto-de-origen? xs)))



(regresan-al-punto-de-origen?)
(regresan-al-punto-de-origen? [])
(regresan-al-punto-de-origen? "")
(regresan-al-punto-de-origen? [] "" (list))
(regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) "")
(regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v))

(regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])
(regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")
(regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])

(defn regreso-al-punto-de-origen
  [s]
  (letfn [(g [xs]
            (if (regresa-al-punto-de-origen? xs)
              (list)
              (f xs "")))
          (h [c]
            (cond
              (= c \<) (str \>)
              (= c \>) (str \<)
              (= c \^) (str \v)
              (= c \v) (str \^)
              :else (str)))
          (f [st c]
            (if (empty? st) (str c)
                (str c (f (drop-last st) (h (last st))))))]
    (g s)))

(regreso-al-punto-de-origen "")
(regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))


(regreso-al-punto-de-origen ">>>")
(regreso-al-punto-de-origen [\< \v \v \v \> \>])


(defn mismo-punto-final?
  [xs ys]
  (letfn [(k [cx x]
            (cond
              (= cx \>) (inc x)
              (= cx \<) (dec x)
              :else x))
          (l [cy y]
            (cond
              (= cy \^) (inc y)
              (= cy \v) (dec y)
              :else y))
          (m [cx x]
            (cond
              (= cx \>) (inc x)
              (= cx \<) (dec x)
              :else x))
          (n [cy y]
            (cond
              (= cy \^) (inc y)
              (= cy \v) (dec y)
              :else y))
          (j [xs ys]
            (and (empty? xs) (empty? ys)))
          (h [a b c d]
            (and (== a c) (== b d)))
          (f [xs ys x y z w]
            (if (j xs ys) (h x y z w)
                (f (rest xs) (rest ys)
                   (k (first xs) x) (l (first xs) y)
                   (m (first ys) z) (n (first ys) w))))]
    (f xs ys 0 0 0 0)))


(mismo-punto-final? "" [])
(mismo-punto-final? "^^^" "<^^^>")
(mismo-punto-final? [\< \< \< \>] (list \< \<))
(mismo-punto-final? (list \< \v \>) (list \> \v \<))

(mismo-punto-final? "" "<")
(mismo-punto-final? [\> \>] "<>")
(mismo-punto-final? [\> \> \>] [\> \> \> \>])
(mismo-punto-final? (list) (list \^))

(defn coincidencias
  [xs ys]
  (letfn [(k [cx x]
            (cond
              (= cx \>) (inc x)
              (= cx \<) (dec x)
              :else x))
          (l [cy y]
            (cond
              (= cy \^) (inc y)
              (= cy \v) (dec y)
              :else y))
          (m [cx x]
            (cond
              (= cx \>) (inc x)
              (= cx \<) (dec x)
              :else x))
          (n [cy y]
            (cond
              (= cy \^) (inc y)
              (= cy \v) (dec y)
              :else y))
          (j [xs ys]
            (and (empty? xs) (empty? ys)))
          (i [a b c d]
            (and (== a c) (== b d)))
          (h [ac a b c d]
            (if (i a b c d) (inc ac) ac))
          (f [xs ys x y z w acc]
            (if (j xs ys) (h acc x y z w)
                (f (rest xs) (rest ys)
                   (k (first xs) x) (l (first xs) y)
                   (m (first ys) z) (n (first ys) w)
                   (h acc x y z w))))]
    (f xs ys 0 0 0 0 0)))

(coincidencias "" [])
(coincidencias (list \< \<) [\> \>])

(coincidencias [\^ \> \> \> \^] ">^^<")
(coincidencias "<<vv>>^>>" "vv<^")
(coincidencias ">>>>>" [\> \> \> \> \>])
(coincidencias [\> \> \> \> \>] (list \> \> \> \> \> \> \^ \^ \^ \^))









































